package com.fhj.gmall.item.config.thread.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app.threadpool")
@Data
public class AppThreadProperties {
    private Integer corePoolSize;
    private Integer maximumPoolSize;
    private Long keepAliveTIme;
    private Integer workQueueSize;

}
