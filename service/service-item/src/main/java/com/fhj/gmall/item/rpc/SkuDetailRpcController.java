package com.fhj.gmall.item.rpc;


import com.baomidou.mybatisplus.extension.api.R;
import com.fhj.gmall.common.result.Result;
import com.fhj.gmall.item.service.SkuDetailService;
import com.fhj.gmall.product.vo.SkuDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * sku详情
 */
@RequestMapping("/api/inner/rpc/item")
@RestController
public class SkuDetailRpcController {
    
    @Autowired
    SkuDetailService skuDetailService;
    /**
     * 获取商品详情数据
     * @param skuId
     * @return
     */
    @GetMapping("/sku/detail/{skuId}")
    public Result<SkuDetailVo> getSkuDetails(@PathVariable("skuId") Long skuId){
    
        SkuDetailVo skuDetailVo =  skuDetailService.getSkuDetailData(skuId);
        return Result.ok(skuDetailVo);
        
    }
}
