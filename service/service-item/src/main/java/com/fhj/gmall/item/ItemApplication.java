package com.fhj.gmall.item;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
//import com.fhj.gmall.common.config.thread.annotation.EnableAppThreadPool;

//@EnableAppThreadPool
@EnableFeignClients
@SpringCloudApplication
public class ItemApplication {
    public static void main(String[] args) {
        SpringApplication.run(ItemApplication.class,args);
    }
}
