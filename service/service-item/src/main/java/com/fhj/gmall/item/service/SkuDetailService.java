package com.fhj.gmall.item.service;

import com.fhj.gmall.product.vo.SkuDetailVo;

public interface SkuDetailService {
    
    /**
     * 获取商品详情数据
     * @param skuId
     * @return
     */
    SkuDetailVo getSkuDetailData(Long skuId);
}
