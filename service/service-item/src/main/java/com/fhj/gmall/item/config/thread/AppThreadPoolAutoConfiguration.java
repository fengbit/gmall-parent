package com.fhj.gmall.item.config.thread;


import com.fhj.gmall.item.config.thread.properties.AppThreadProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


@Slf4j
@Configuration
@EnableConfigurationProperties(AppThreadProperties.class)
public class AppThreadPoolAutoConfiguration {
    
    
    @Bean
    public ThreadPoolExecutor coreExecutor(AppThreadProperties threadProperties) {
        /*
              int corePoolSize,
              int maximumPoolSize,
              long keepAliveTime,
              TimeUnit unit,
              BlockingQueue<Runnable> workQueue,
              ThreadFactory threadFactory,
              RejectedExecutionHandler handler) {
         */
        return new ThreadPoolExecutor(
                threadProperties.getCorePoolSize(),
                threadProperties.getMaximumPoolSize(),
                threadProperties.getKeepAliveTIme(), TimeUnit.MINUTES,
                new LinkedBlockingQueue<>(threadProperties.getWorkQueueSize()),
                new ThreadFactory() {
                    int i = 1;
                    
                    @Override
                    public Thread newThread(Runnable r) {
                        log.info("线程池：准备新线程；老线程会复用");
                        Thread thread = new Thread(r);
                        thread.setName("线程池：" + i++ + "线程");
                        thread.setPriority(10);
                        return thread;
                    }
                },
                new ThreadPoolExecutor.CallerRunsPolicy()
                
                );
    }
    
    
    
    
//    @Bean
//    public ThreadPoolExecutor otherExecutor() {
//        /*
//              int corePoolSize,
//              int maximumPoolSize,
//              long keepAliveTime,
//              TimeUnit unit,
//              BlockingQueue<Runnable> workQueue,
//              ThreadFactory threadFactory,
//              RejectedExecutionHandler handler) {
//         */
//        return new ThreadPoolExecutor(8, 24, 5, TimeUnit.MINUTES,
//                new LinkedBlockingQueue<>(3000),
//                new ThreadFactory() {
//                    int i = 1;
//
//                    @Override
//                    public Thread newThread(Runnable r) {
//                        log.info("线程池：准备新线程；老线程会复用");
//                        Thread thread = new Thread(u);
//                        thread.setName("线程池：" + i++ + "线程");
//                        thread.setPriority();
//                        return thread;
//                    }
//                });
//    }
    
}
