package com.fhj.gmall.product.mapper;

import com.fhj.gmall.product.entity.BaseTrademark;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author q2731557667
* @description 针对表【base_trademark(品牌表)】的数据库操作Mapper
* @createDate 2024-02-16 19:27:50
* @Entity com.fhj.gmall.product.entity.BaseTrademark
*/
public interface BaseTrademarkMapper extends BaseMapper<BaseTrademark> {

}




