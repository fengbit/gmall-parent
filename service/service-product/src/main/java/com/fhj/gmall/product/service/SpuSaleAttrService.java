package com.fhj.gmall.product.service;

import com.fhj.gmall.product.entity.SpuSaleAttr;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【spu_sale_attr(spu销售属性)】的数据库操作Service
* @createDate 2024-02-16 19:27:50
*/
public interface SpuSaleAttrService extends IService<SpuSaleAttr> {
    
    
    List<SpuSaleAttr> getSpuSaleAttrList(Long spuId);
    
    List<SpuSaleAttr> getSpuSaleAttrListOrder(Long spuId,Long skuId);
    
    /**
     * 根据spuid得到所有sku的销售属性组合
     * @param spuId
     * @return
     */
    String getValuesSkuJson(Long spuId);
}
