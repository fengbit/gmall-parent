package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.SpuImage;
import com.fhj.gmall.product.service.SpuImageService;
import com.fhj.gmall.product.mapper.SpuImageMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【spu_image(商品图片表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class SpuImageServiceImpl extends ServiceImpl<SpuImageMapper, SpuImage>
    implements SpuImageService{

}




