package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.SpuPoster;
import com.fhj.gmall.product.service.SpuPosterService;
import com.fhj.gmall.product.mapper.SpuPosterMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【spu_poster(商品海报表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class SpuPosterServiceImpl extends ServiceImpl<SpuPosterMapper, SpuPoster>
    implements SpuPosterService{

}




