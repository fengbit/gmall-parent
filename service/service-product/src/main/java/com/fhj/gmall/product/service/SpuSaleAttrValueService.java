package com.fhj.gmall.product.service;

import com.fhj.gmall.product.entity.SpuSaleAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author q2731557667
* @description 针对表【spu_sale_attr_value(spu销售属性值)】的数据库操作Service
* @createDate 2024-02-16 19:27:50
*/
public interface SpuSaleAttrValueService extends IService<SpuSaleAttrValue> {

}
