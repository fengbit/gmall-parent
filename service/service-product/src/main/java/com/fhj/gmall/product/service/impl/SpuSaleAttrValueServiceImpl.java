package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.SpuSaleAttrValue;
import com.fhj.gmall.product.service.SpuSaleAttrValueService;
import com.fhj.gmall.product.mapper.SpuSaleAttrValueMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【spu_sale_attr_value(spu销售属性值)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class SpuSaleAttrValueServiceImpl extends ServiceImpl<SpuSaleAttrValueMapper, SpuSaleAttrValue>
    implements SpuSaleAttrValueService{

}




