package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.BaseCategory2;
import com.fhj.gmall.product.entity.BaseCategory3;
import com.fhj.gmall.product.service.BaseCategory3Service;
import com.fhj.gmall.product.mapper.BaseCategory3Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【base_category3(三级分类表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class BaseCategory3ServiceImpl extends ServiceImpl<BaseCategory3Mapper, BaseCategory3>
    implements BaseCategory3Service{
    
    @Override
    public List<BaseCategory3> getCategory3sByClId(Long category2Id) {
        QueryWrapper<BaseCategory3> wrapper = new QueryWrapper<>();
        wrapper.eq("category2_id",category2Id);
        List<BaseCategory3> list = this.list(wrapper);
        return list;
    }
}




