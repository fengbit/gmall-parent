package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.BaseCategory2;
import com.fhj.gmall.product.service.BaseCategory2Service;
import com.fhj.gmall.product.mapper.BaseCategory2Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【base_category2(二级分类表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class BaseCategory2ServiceImpl extends ServiceImpl<BaseCategory2Mapper, BaseCategory2>
    implements BaseCategory2Service{
    
    @Override
    public List<BaseCategory2> getCategory2sByClId(Long category1Id) {
    
        QueryWrapper<BaseCategory2> wrapper = new QueryWrapper<>();
        wrapper.eq("category1_id",category1Id);
        List<BaseCategory2> list = this.list(wrapper);
        return list;
    }
}




