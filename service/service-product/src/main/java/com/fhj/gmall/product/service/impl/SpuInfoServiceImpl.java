package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.SpuInfo;
import com.fhj.gmall.product.service.SpuInfoService;
import com.fhj.gmall.product.mapper.SpuInfoMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【spu_info(商品表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoMapper, SpuInfo>
    implements SpuInfoService{

}




