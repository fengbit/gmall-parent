package com.fhj.gmall.product;


import com.fhj.gmall.common.config.Swagger2Config;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@Import(Swagger2Config.class)
@SpringCloudApplication
@ComponentScan({"com.fhj.gmall"})
@MapperScan(basePackages = "com/fhj/gmall/product/mapper")
public class ProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class,args);
    }
}
