package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.BaseAttrInfo;
import com.fhj.gmall.product.service.BaseAttrInfoService;
import com.fhj.gmall.product.mapper.BaseAttrInfoMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【base_attr_info(属性表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class BaseAttrInfoServiceImpl extends ServiceImpl<BaseAttrInfoMapper, BaseAttrInfo>
    implements BaseAttrInfoService{
    
    @Override
    public List<BaseAttrInfo> getBaseAttrAndValue(Long category1Id, Long category2Id, Long category3Id) {
       
        return  baseMapper.getBaseAttrAndValue(category1Id,category2Id,category3Id);
    }
}




