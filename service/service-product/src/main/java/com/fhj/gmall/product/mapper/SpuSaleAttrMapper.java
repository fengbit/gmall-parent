package com.fhj.gmall.product.mapper;

import com.fhj.gmall.product.entity.SpuSaleAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fhj.gmall.product.vo.ValueSkuJsonVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【spu_sale_attr(spu销售属性)】的数据库操作Mapper
* @Entity com.fhj.gmall.product.entity.SpuSaleAttr
*/
public interface SpuSaleAttrMapper extends BaseMapper<SpuSaleAttr> {
    
    List<SpuSaleAttr> getSpuSaleAttrList(@Param("spuId") Long spuId);
    
    List<SpuSaleAttr> getSpuSaleAttrListOrder(@Param("spuId") Long spuId,@Param("skuId")Long skuId);
    
    List<ValueSkuJsonVo> getValuesSkuJson(@Param("spuId") Long spuId);
}




