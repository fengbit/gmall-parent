package com.fhj.gmall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.SpuSaleAttr;
import com.fhj.gmall.product.service.SpuSaleAttrService;
import com.fhj.gmall.product.mapper.SpuSaleAttrMapper;
import com.fhj.gmall.product.vo.ValueSkuJsonVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
* @author q2731557667
* @description 针对表【spu_sale_attr(spu销售属性)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class SpuSaleAttrServiceImpl extends ServiceImpl<SpuSaleAttrMapper, SpuSaleAttr>
    implements SpuSaleAttrService{
    
    
    /**
     * 查询spu销售属性名和值的集合
     * @param spuId
     * @return
     */
    @Override
    public List<SpuSaleAttr> getSpuSaleAttrList(Long spuId) {
        List<SpuSaleAttr> saleAttrs =  baseMapper.getSpuSaleAttrList(spuId);
        return saleAttrs;
    }
    
    @Override
    public List<SpuSaleAttr> getSpuSaleAttrListOrder(Long spuId,Long skuId) {
        List<SpuSaleAttr> saleAttrs = baseMapper.getSpuSaleAttrListOrder(spuId,skuId);
        
        return saleAttrs;
    }
    
    @Override
    public String getValuesSkuJson(Long spuId) {
        List<ValueSkuJsonVo> vos = baseMapper.getValuesSkuJson(spuId);
        
        Map<String, Long> map = vos.stream()
                .collect(Collectors.toMap(ValueSkuJsonVo::getValueJson, ValueSkuJsonVo::getSkuId));
        String jsonString = JSON.toJSONString(map);
        return jsonString;
    }
}




