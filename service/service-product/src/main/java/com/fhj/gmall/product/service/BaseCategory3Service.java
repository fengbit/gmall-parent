package com.fhj.gmall.product.service;

import com.fhj.gmall.product.entity.BaseCategory3;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【base_category3(三级分类表)】的数据库操作Service
* @createDate 2024-02-16 19:27:50
*/
public interface BaseCategory3Service extends IService<BaseCategory3> {
    
    List<BaseCategory3> getCategory3sByClId(Long category2Id);
}
