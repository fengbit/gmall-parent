package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.SkuImage;
import com.fhj.gmall.product.service.SkuImageService;
import com.fhj.gmall.product.mapper.SkuImageMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【sku_image(库存单元图片表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class SkuImageServiceImpl extends ServiceImpl<SkuImageMapper, SkuImage>
    implements SkuImageService{

}




