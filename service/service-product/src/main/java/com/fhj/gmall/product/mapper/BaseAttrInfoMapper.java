package com.fhj.gmall.product.mapper;

import com.fhj.gmall.product.entity.BaseAttrInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author q2731557667
 * @description 针对表【base_attr_info(属性表)】的数据库操作Mapper
 * @createDate 2024-02-16 19:27:50
 * @Entity com.fhj.gmall.product.entity.BaseAttrInfo
 */
public interface BaseAttrInfoMapper extends BaseMapper<BaseAttrInfo> {
    
    List<BaseAttrInfo> getBaseAttrAndValue(
            @Param("category1Id") Long category1Id,
            @Param("category2Id") Long category2Id,
            @Param("category3Id") Long category3Id);
}




