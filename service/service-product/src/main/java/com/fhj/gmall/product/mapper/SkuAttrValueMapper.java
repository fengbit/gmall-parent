package com.fhj.gmall.product.mapper;

import com.fhj.gmall.product.entity.SkuAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author q2731557667
* @description 针对表【sku_attr_value(sku平台属性值关联表)】的数据库操作Mapper
* @createDate 2024-02-16 19:27:50
* @Entity com.fhj.gmall.product.entity.SkuAttrValue
*/
public interface SkuAttrValueMapper extends BaseMapper<SkuAttrValue> {

}




