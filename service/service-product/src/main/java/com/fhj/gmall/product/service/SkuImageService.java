package com.fhj.gmall.product.service;

import com.fhj.gmall.product.entity.SkuImage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author q2731557667
* @description 针对表【sku_image(库存单元图片表)】的数据库操作Service
* @createDate 2024-02-16 19:27:50
*/
public interface SkuImageService extends IService<SkuImage> {

}
