package com.fhj.gmall.product.controller;


import com.fhj.gmall.common.result.Result;
import com.fhj.gmall.product.entity.BaseAttrInfo;
import com.fhj.gmall.product.service.BaseAttrInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*
平台属性
 */
@Api(tags = "平台属性")
@RestController
@RequestMapping("/admin/product")
public class BaseAttrController {
    
    @Autowired
    BaseAttrInfoService baseAttrInfoService;
    
    @ApiOperation("根据分类id获取平台属性")
    @GetMapping("/attrInfoList/{category1Id}/{category2Id}/{category3Id}")
    public Result getBaseAttr(@PathVariable("category1Id") Long category1Id,
                              @PathVariable("category2Id") Long category2Id,
                              @PathVariable("category3Id") Long category3Id) {
        
        List<BaseAttrInfo> attrInfos =  baseAttrInfoService.getBaseAttrAndValue(category1Id,category2Id,category3Id);
        
        return Result.ok(attrInfos);
    }
}
