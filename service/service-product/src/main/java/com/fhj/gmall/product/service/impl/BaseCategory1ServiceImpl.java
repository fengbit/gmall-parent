package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.BaseCategory1;
import com.fhj.gmall.product.mapper.BaseCategory1Mapper;
import com.fhj.gmall.product.mapper.BaseCategory2Mapper;
import com.fhj.gmall.product.service.BaseCategoryService;
import com.fhj.gmall.product.vo.CategoryTreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BaseCategory1ServiceImpl extends ServiceImpl<BaseCategory1Mapper, BaseCategory1>
        implements BaseCategoryService {
    
    
    @Autowired
    BaseCategory2Mapper baseCategory2Mapper;
    @Override
    public List<CategoryTreeVo> getCategoryTree() {
    
        List<CategoryTreeVo> vos = baseCategory2Mapper.getCategoryTree();
        
        return vos;
    }
    
    @Override
    public CategoryTreeVo getCategoryTreeWithC3Id(Long c3Id) {
        CategoryTreeVo vo = baseCategory2Mapper.getCategoryTreeWithC3Id(c3Id);
        return vo;
    }
}