package com.fhj.gmall.product.service;

import com.fhj.gmall.product.entity.BaseCategory2;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【base_category2(二级分类表)】的数据库操作Service
* @createDate 2024-02-16 19:27:50
*/
public interface BaseCategory2Service extends IService<BaseCategory2> {
    
    List<BaseCategory2> getCategory2sByClId(Long category1Id);
}
