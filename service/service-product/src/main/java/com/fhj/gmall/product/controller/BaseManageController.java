package com.fhj.gmall.product.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fhj.gmall.common.result.Result;
import com.fhj.gmall.product.entity.BaseCategory1;
import com.fhj.gmall.product.entity.BaseCategory2;
import com.fhj.gmall.product.entity.BaseCategory3;
import com.fhj.gmall.product.service.BaseCategory2Service;
import com.fhj.gmall.product.service.BaseCategory3Service;
import com.fhj.gmall.product.service.BaseCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "三级分类管理")
@RestController
@RequestMapping("admin/product")
public class BaseManageController {
    
    @Autowired
    private BaseCategoryService baseCategoryService;
    @Autowired
    private BaseCategory2Service baseCategory2Service;
    @Autowired
    private BaseCategory3Service baseCategory3Service;
    
    /**
     * 查询所有的一级分类信息
     * admin/product/getCategory1
     * @return
     */
    @ApiOperation("查询所有的一级分类信息")
    @GetMapping("getCategory1")
    public Result<List<BaseCategory1>> getCategory1() {
        List<BaseCategory1> category1s = baseCategoryService.list();
        return Result.ok(category1s);
    }
    
    /**
     *
     * @return
     */
    @ApiOperation("查询所有一级目录下的二级分类信息")
    @GetMapping("getCategory2/{category1Id}")
    public Result<List<BaseCategory2>> getCategory2(@PathVariable("category1Id") Long category1Id) {
        List<BaseCategory2> list=  baseCategory2Service.getCategory2sByClId(category1Id);
        return Result.ok(list);
    }
    
    /**
     *
     * @return
     */
    @ApiOperation("查询所有二级目录下的三级分类信息")
    @GetMapping("getCategory3/{category2Id}")
    public Result<List<BaseCategory3>> getCategory3(@PathVariable("category2Id") Long category2Id) {
        List<BaseCategory3> list=  baseCategory3Service.getCategory3sByClId(category2Id);
        return Result.ok(list);
    }
    
}
