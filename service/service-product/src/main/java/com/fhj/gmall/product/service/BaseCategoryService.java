package com.fhj.gmall.product.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.fhj.gmall.product.entity.BaseCategory1;
import com.fhj.gmall.product.vo.CategoryTreeVo;

import java.util.List;


public interface BaseCategoryService extends IService<BaseCategory1> {
    
    List<CategoryTreeVo> getCategoryTree();
    
    CategoryTreeVo getCategoryTreeWithC3Id(Long c3Id);
}
