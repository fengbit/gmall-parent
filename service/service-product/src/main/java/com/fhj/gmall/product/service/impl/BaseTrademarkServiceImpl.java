package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.BaseTrademark;
import com.fhj.gmall.product.service.BaseTrademarkService;
import com.fhj.gmall.product.mapper.BaseTrademarkMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【base_trademark(品牌表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class BaseTrademarkServiceImpl extends ServiceImpl<BaseTrademarkMapper, BaseTrademark>
    implements BaseTrademarkService{

}




