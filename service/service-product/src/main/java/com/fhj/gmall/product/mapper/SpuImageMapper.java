package com.fhj.gmall.product.mapper;

import com.fhj.gmall.product.entity.SpuImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author q2731557667
* @description 针对表【spu_image(商品图片表)】的数据库操作Mapper
* @createDate 2024-02-16 19:27:50
* @Entity com.fhj.gmall.product.entity.SpuImage
*/
public interface SpuImageMapper extends BaseMapper<SpuImage> {

}




