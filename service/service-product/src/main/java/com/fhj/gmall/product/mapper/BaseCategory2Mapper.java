package com.fhj.gmall.product.mapper;

import com.fhj.gmall.product.entity.BaseCategory2;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fhj.gmall.product.vo.CategoryTreeVo;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【base_category2(二级分类表)】的数据库操作Mapper
* @createDate 2024-02-16 19:27:50
* @Entity com.fhj.gmall.product.entity.BaseCategory2
*/
public interface BaseCategory2Mapper extends BaseMapper<BaseCategory2> {
    
    List<CategoryTreeVo> getCategoryTree();
    
    CategoryTreeVo getCategoryTreeWithC3Id(Long c3Id);
}




