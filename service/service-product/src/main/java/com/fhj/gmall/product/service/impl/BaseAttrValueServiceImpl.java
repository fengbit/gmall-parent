package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.BaseAttrValue;
import com.fhj.gmall.product.service.BaseAttrValueService;
import com.fhj.gmall.product.mapper.BaseAttrValueMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【base_attr_value(属性值表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class BaseAttrValueServiceImpl extends ServiceImpl<BaseAttrValueMapper, BaseAttrValue>
    implements BaseAttrValueService{

}




