package com.fhj.gmall.product.service;

import com.fhj.gmall.product.entity.SpuInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author q2731557667
* @description 针对表【spu_info(商品表)】的数据库操作Service
* @createDate 2024-02-16 19:27:50
*/
public interface SpuInfoService extends IService<SpuInfo> {

}
