package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.SkuAttrValue;
import com.fhj.gmall.product.service.SkuAttrValueService;
import com.fhj.gmall.product.mapper.SkuAttrValueMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【sku_attr_value(sku平台属性值关联表)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class SkuAttrValueServiceImpl extends ServiceImpl<SkuAttrValueMapper, SkuAttrValue>
    implements SkuAttrValueService{

}




