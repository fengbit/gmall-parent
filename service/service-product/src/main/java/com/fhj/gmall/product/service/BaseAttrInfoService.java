package com.fhj.gmall.product.service;

import com.fhj.gmall.product.entity.BaseAttrInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author q2731557667
* @description 针对表【base_attr_info(属性表)】的数据库操作Service
* @createDate 2024-02-16 19:27:50
*/
public interface BaseAttrInfoService extends IService<BaseAttrInfo> {
    
    List<BaseAttrInfo> getBaseAttrAndValue(Long category1Id, Long category2Id, Long category3Id);
}
