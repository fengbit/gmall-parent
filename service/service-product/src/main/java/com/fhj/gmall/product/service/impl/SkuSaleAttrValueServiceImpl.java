package com.fhj.gmall.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fhj.gmall.product.entity.SkuSaleAttrValue;
import com.fhj.gmall.product.service.SkuSaleAttrValueService;
import com.fhj.gmall.product.mapper.SkuSaleAttrValueMapper;
import org.springframework.stereotype.Service;

/**
* @author q2731557667
* @description 针对表【sku_sale_attr_value(sku销售属性值)】的数据库操作Service实现
* @createDate 2024-02-16 19:27:50
*/
@Service
public class SkuSaleAttrValueServiceImpl extends ServiceImpl<SkuSaleAttrValueMapper, SkuSaleAttrValue>
    implements SkuSaleAttrValueService{

}




