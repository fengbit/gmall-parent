package com.fhj.gmall.web.feign;


import com.fhj.gmall.common.result.Result;
import com.fhj.gmall.product.vo.SkuDetailVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/inner/rpc/item")
@FeignClient("service-item") //调用详情服务
public interface SkuDetailFeignClient {
    /**
     * 获取商品详情数据
     * @param skuId
     * @return
     */
    @GetMapping("/sku/detail/{skuId}")
    Result<SkuDetailVo> getSkuDetails(@PathVariable("skuId") Long skuId);
}
