package com.fhj.gmall.web.controller;


import com.fhj.gmall.common.result.Result;
import com.fhj.gmall.product.vo.CategoryTreeVo;
import com.fhj.gmall.web.feign.CategoryFeignClient;
import com.sun.xml.internal.bind.v2.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class IndexController {
    
    @Autowired
    CategoryFeignClient categoryFeignClient;
    
    @GetMapping("/")
    public String index(Model model) {
        
        //远程调用 service-product 获取系统所有的三级分类数据
        Result<List<CategoryTreeVo>> categorys = categoryFeignClient.getCategoryTree();
        List<CategoryTreeVo> data = categorys.getData();
        model.addAttribute("list",data);
        return "index/index";
    }
}
